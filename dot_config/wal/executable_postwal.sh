#!/bin/bash
echo "-- Running postwal.sh --"

"${HOME}/.cache/wal/colors.sh"

echo "-- Reloading qutebrowser --"
$HOME/.config/wal/qutebrowser_reload.py

echo "-- Reload sway --"
swaymsg reload

echo "-- Restart waybar and dunst --"
pkill waybar
pkill dunst
waybar &
dunst &
