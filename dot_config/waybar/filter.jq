 def roundto(n):   (./n|floor)*n ;
 def timefmt(s):
   def calcsecs:                             .s_to_m = (.seconds | roundto(60)) | .secs = .seconds - .s_to_m ;
   def calcmins:     .minutes = .s_to_m/60 | .m_to_h = (.minutes | roundto(60)) | .mins = .minutes - .m_to_h ;
   def calchrs:      .hours   = .m_to_h/60 | .h_to_d = (.hours   | roundto(24)) | .hrs  = .hours   - .h_to_d ;
   def calcdays:     .days    = .h_to_d/24 ;
   def fmtelt(e;u):  if e>0 then " \(e)\(u)" else "" end ;
   def fmt:
       .s = " \(.secs)s"
     | .m = fmtelt(.mins; "m")
     | .h = fmtelt(.hrs; "h")
     | .d = fmtelt(.days; "d")
     | "\(.d)\(.h)\(.m)\(.s)"[1:]
   ;
   {seconds: (s)|floor} | calcsecs | calcmins | calchrs | calcdays | fmt
;
timefmt($s)
