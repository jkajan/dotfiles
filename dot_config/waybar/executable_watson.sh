#!/bin/sh

strip() {
  echo $@ | sed -r 's/^No project started.$//'
}
TASK=$(strip $(watson status -p))
TAG=$(strip $(watson status -t))
ELAPSED=$(strip $(watson status -e))

LOG=$(watson log --json --current --day)
#CURRENT=$(echo $LOG | jq '.[length-1] | select(.id == "current")')
JQ_TEXT='. | {
  text: .[length-1] | (.project +" ["+ (.tags | join(", ") ) + "]"),
  alt: .[length-1] | .id,
  class: .[length-1] | .id
}'
#tooltip: [(.[] | ((.project +" [" + .start +"]")))] | join("\n")
TEXT=$(echo $LOG | jq -jc --unbuffered "$JQ_TEXT")

AGGREGATE=$(watson aggregate --json --current --from $(date +%Y-%m-%d))
#JQ_TOOLTIP='.[].projects | del(.[].tags) | map({(.name):(.time) }) | add | {tooltip: (.)}'
JQ_TOOLTIP='.[].projects | del(.[].tags) | .[] | [(.name)+" -- "+(.time|tostring)]'
TOOLTIP=$(echo $AGGREGATE | jq "$JQ_TOOLTIP")
#echo $TEXT '{tooltip:"'$TOOLTIP'"}' | jq -jc --unbuffered -s 'add'
echo $TEXT
